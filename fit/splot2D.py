from ROOT import *

gROOT.Reset()
gROOT.SetStyle("Plain")
TProof.Open("")


minMassJpsi = 3000
maxMassJpsi = 4000
binWidthJpsi = 10.
binNJpsi = int((maxMassJpsi-minMassJpsi)/binWidthJpsi)


minMassLst = 1440.
maxMassLst = 1590.
binWidthLst = 2.
binNLst = int((maxMassLst-minMassLst)/binWidthLst)



Jpsi_M = RooRealVar("Jpsi_M", "Jpsi_M", minMassJpsi, maxMassJpsi, "MeV")
Lst1_M = RooRealVar("Lst1_M", "Lst1_M", minMassLst, maxMassLst, "MeV")
Lst2_M = RooRealVar("Lst2_M", "Lst2_M", minMassLst, maxMassLst, "MeV")
    
    
chain = TChain("DecayTree")
chain.Add("../Data/LstLst_RunII_cutDef_tightPID.root")



dsetFull = RooDataSet("dsetFull", "dsetFull", chain, RooArgSet(Jpsi_M, Lst1_M, Lst2_M))

LstMass = 1519.5
varLstMass = RooRealVar("varLstMass", "varLstMass", LstMass, LstMass-10, LstMass+10)


varSigma = RooRealVar("varSigma", "varSigma", 1, 0.1, 15)
varLstGamma = RooRealVar("varLstGamma", "varLstGamma", 15.6)
varKpMass = RooRealVar ("varKpMass", "varKpMass", 493.67+938.27)
    
    
varA1 = RooRealVar("varA1", "varA1", -1,1)
varA2 = RooRealVar("varA2", "varA2", -1,1)

varB1 = RooRealVar("varB1", "varB1", 0)
varB2 = RooRealVar("varB2", "varB2", 0)    
    
pdfLst1 = RooGenericPdf("pdfLst1", "pdfLst1", "sqrt(@0-@1)*TMath::Voigt(@0-@2,@3,@4)",RooArgList(Lst1_M, varKpMass, varLstMass,varSigma,varLstGamma))
pdfLst2 = RooGenericPdf("pdfLst2", "pdfLst2", "sqrt(@0-@1)*TMath::Voigt(@0-@2,@3,@4)",RooArgList(Lst2_M, varKpMass, varLstMass,varSigma,varLstGamma))

pdfRoot1A1 = RooGenericPdf("pdfRoot1A1", "pdfRoot1A1", "sqrt(@0-@1)*(1+(@0-@1)*@2+(@0-@1)*(@0-@1)*@3)", RooArgList(Lst1_M, varKpMass,varA1,varB1))
pdfRoot1A2 = RooGenericPdf("pdfRoot1A2", "pdfRoot1A2", "sqrt(@0-@1)*(1+(@0-@1)*@2+(@0-@1)*(@0-@1)*@3)", RooArgList(Lst1_M, varKpMass,varA2,varB2))
pdfRoot2A1 = RooGenericPdf("pdfRoot2A1", "pdfRoot2A1", "sqrt(@0-@1)*(1+(@0-@1)*@2+(@0-@1)*(@0-@1)*@3)", RooArgList(Lst2_M, varKpMass,varA1,varB1))
pdfRoot2A2 = RooGenericPdf("pdfRoot2A2", "pdfRoot2A2", "sqrt(@0-@1)*(1+(@0-@1)*@2+(@0-@1)*(@0-@1)*@3)", RooArgList(Lst2_M, varKpMass,varA2,varB2))
                          

pdfSS = RooProdPdf("pdfSS", "pdfSS", RooArgList(pdfLst1, pdfLst2))
pdfSB = RooProdPdf("pdfSB", "pdfSB", RooArgList(pdfLst1, pdfRoot2A1))
pdfBS = RooProdPdf("pdfBS", "pdfBS", RooArgList(pdfRoot1A1, pdfLst2))
pdfBB = RooProdPdf("pdfBB", "pdfBB", RooArgList(pdfRoot1A2, pdfRoot2A2))

                          
                          
varNDiLst = RooRealVar("varNDiLst", "varNDiLst", 0, 1e7)
varNLstKp  = RooRealVar("varNLstKp", "varNLstKp", 0, 1e7)
varNKpKp   = RooRealVar ("varNKpKp", "varNKpKp", 0, 1e7)
                          
                          
                          

pdfModel1 = RooAddPdf("pdfModel1", "pdfModel1",  RooArgList(pdfLst1,pdfLst1,pdfRoot1A1,pdfRoot1A2),
                                              RooArgList(varNDiLst,varNLstKp,varNLstKp,varNKpKp))
pdfModel2 = RooAddPdf("pdfModel2", "pdfModel2",  RooArgList(pdfLst2,pdfRoot2A1,pdfLst2,pdfRoot2A2),
                                              RooArgList(varNDiLst,varNLstKp,varNLstKp,varNKpKp))
                          
                          
pdfModel = RooAddPdf("pdfModel", "pdfModel",  RooArgList(pdfSS,pdfSB,pdfBS,pdfBB), RooArgList(varNDiLst,varNLstKp,varNLstKp,varNKpKp))
                          
                          
                          
res = pdfModel.fitTo(dsetFull, RooFit.Save(True), RooFit.PrintLevel(0),RooFit.Extended())
res = pdfModel.fitTo(dsetFull, RooFit.Save(True), RooFit.PrintLevel(0),RooFit.Extended())
varLstMass.setConstant(True)
varSigma.setConstant(True)
varA1.setConstant(True)
varA2.setConstant(True)


frame1 = Lst1_M.frame(RooFit.Title("#Lst_{1} mass"))
dsetFull.plotOn(frame1, RooFit.Binning(binNLst, minMassLst, maxMassLst))
pdfModel.plotOn(frame1)

frame2 = Lst2_M.frame(RooFit.Title("#Lst_{2} mass"))
dsetFull.plotOn(frame2, RooFit.Binning(binNLst, minMassLst, maxMassLst))
pdfModel.plotOn(frame2)


canvA = TCanvas("canvA", "canvA", 1000, 400)
canvA.Divide(2,1)
canvA.cd(1)
frame1.Draw()
canvA.cd(2)
frame2.Draw()
canvA.SaveAs("KpKpMass.pdf")






sData1 = RooStats.SPlot("sData1", "sData1", dsetFull, pdfModel, RooArgList(varNDiLst,varNLstKp,varNKpKp))
dset = RooDataSet(dsetFull.GetName(),dsetFull.GetTitle(),dsetFull,dsetFull.get(),"","varNDiLst_sw")


frame3 = Jpsi_M.frame(RooFit.Title("LstLst mass"))
dset.plotOn(frame3,RooFit.Binning(binNJpsi, minMassJpsi, maxMassJpsi),RooFit.DataError(RooAbsData.SumW2))
canvB = TCanvas("canvB", "canvB", 800, 400)
frame3.Draw()
canvB.SaveAs("Jpsi_M.pdf")



file = TFile("SplotLstLst.root","recreate")
tree = TTree(dset.tree())
tree.SetName("DecayTree")
tree.SetTitle("DecayTree")
tree.Write()
file.Close()



