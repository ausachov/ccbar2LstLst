from ROOT import *

gROOT.Reset()
gROOT.SetStyle("Plain")
TProof.Open("")


minMassJpsi = 3000
maxMassJpsi = 4000
binWidthJpsi = 20.
binNJpsi = int((maxMassJpsi-minMassJpsi)/binWidthJpsi)


minMassLst = 1440.
maxMassLst = 1590.
binWidthLst = 2.
binNLst = int((maxMassLst-minMassLst)/binWidthLst)



Jpsi_M = RooRealVar("Jpsi_M", "Jpsi_M", minMassJpsi, maxMassJpsi, "MeV")
Lst1_M = RooRealVar("Lst1_M", "Lst1_M", minMassLst, maxMassLst, "MeV")
Lst2_M = RooRealVar("Lst2_M", "Lst2_M", minMassLst, maxMassLst, "MeV")
    
    
chain = TChain("DecayTree")
#chain.Add("../Data/LstLst_RunII_cutDef_tightPID.root")
chain.Add("../Data/LstLst_RunII_cutDef_v0.root")


dsetFull = RooDataSet("dsetFull", "dsetFull", chain, RooArgSet(Jpsi_M, Lst1_M, Lst2_M))

LstMass = 1519.5
varLstMass = RooRealVar("varLstMass", "varLstMass", LstMass, LstMass-10, LstMass+10)


varSigma = RooRealVar("varSigma", "varSigma", 1, 0.1, 15)
varLstGamma = RooRealVar("varLstGamma", "varLstGamma", 15.6)
varKpMass = RooRealVar ("varKpMass", "varKpMass", 493.67+938.27)
    
    
varA1 = RooRealVar("varA1", "varA1", -1,1)
varA2 = RooRealVar("varA2", "varA2", -1,1)

varB1 = RooRealVar("varB1", "varB1", -1,1)
varB2 = RooRealVar("varB2", "varB2", -1,1)    
    
pdfLst1 = RooGenericPdf("pdfLst1", "pdfLst1", "sqrt(@0-@1)*TMath::Voigt(@0-@2,@3,@4)",RooArgList(Lst1_M, varKpMass, varLstMass,varSigma,varLstGamma))
pdfLst2 = RooGenericPdf("pdfLst2", "pdfLst2", "sqrt(@0-@1)*TMath::Voigt(@0-@2,@3,@4)",RooArgList(Lst2_M, varKpMass, varLstMass,varSigma,varLstGamma))

pdfRoot1A1 = RooGenericPdf("pdfRoot1A1", "pdfRoot1A1", "sqrt(@0-@1)*(1+(@0-@1)*@2+(@0-@1)*(@0-@1)*@3)", RooArgList(Lst1_M, varKpMass,varA1,varB1))
pdfRoot1A2 = RooGenericPdf("pdfRoot1A2", "pdfRoot1A2", "sqrt(@0-@1)*(1+(@0-@1)*@2+(@0-@1)*(@0-@1)*@3)", RooArgList(Lst1_M, varKpMass,varA2,varB2))
pdfRoot2A1 = RooGenericPdf("pdfRoot2A1", "pdfRoot2A1", "sqrt(@0-@1)*(1+(@0-@1)*@2+(@0-@1)*(@0-@1)*@3)", RooArgList(Lst2_M, varKpMass,varA1,varB1))
pdfRoot2A2 = RooGenericPdf("pdfRoot2A2", "pdfRoot2A2", "sqrt(@0-@1)*(1+(@0-@1)*@2+(@0-@1)*(@0-@1)*@3)", RooArgList(Lst2_M, varKpMass,varA2,varB2))
                          

pdfSS = RooProdPdf("pdfSS", "pdfSS", RooArgList(pdfLst1, pdfLst2))
pdfSB = RooProdPdf("pdfSB", "pdfSB", RooArgList(pdfLst1, pdfRoot2A1))
pdfBS = RooProdPdf("pdfBS", "pdfBS", RooArgList(pdfRoot1A1, pdfLst2))
pdfBB = RooProdPdf("pdfBB", "pdfBB", RooArgList(pdfRoot1A2, pdfRoot2A2))

                          
                          
varNDiLst = RooRealVar("varNDiLst", "varNDiLst", -20, 1e7)
varNLstKp  = RooRealVar("varNLstKp", "varNLstKp", -20, 1e7)
varNKpKp   = RooRealVar ("varNKpKp", "varNKpKp", 0, 1e7)
                          
                          
                          

pdfModel1 = RooAddPdf("pdfModel1", "pdfModel1",  RooArgList(pdfLst1,pdfLst1,pdfRoot1A1,pdfRoot1A2),
                                              RooArgList(varNDiLst,varNLstKp,varNLstKp,varNKpKp))
pdfModel2 = RooAddPdf("pdfModel2", "pdfModel2",  RooArgList(pdfLst2,pdfRoot2A1,pdfLst2,pdfRoot2A2),
                                              RooArgList(varNDiLst,varNLstKp,varNLstKp,varNKpKp))
                          
                          
pdfModel = RooAddPdf("pdfModel", "pdfModel",  RooArgList(pdfSS,pdfSB,pdfBS,pdfBB), RooArgList(varNDiLst,varNLstKp,varNLstKp,varNKpKp))
                          

                          
res = pdfModel.fitTo(dsetFull, RooFit.Save(True), RooFit.PrintLevel(0),RooFit.Extended())
res = pdfModel.fitTo(dsetFull, RooFit.Save(True), RooFit.PrintLevel(0),RooFit.Extended())
varLstMass.setConstant(True)
varSigma.setConstant(True)



histLstLst = TH1F("histLstLst", "histLstLst", binNJpsi, minMassJpsi, maxMassJpsi);
for i in range(binNJpsi):
    
    print "Bin ",i," is processing\n"
    
    massJpsiLo = minMassJpsi + i*binWidthJpsi
    massJpsiHi = minMassJpsi + (i+1)*binWidthJpsi
    label =  "Jpsi_M>"+str(massJpsiLo)+"&&Jpsi_M<"+str(massJpsiHi)
    dset = RooDataSet(dsetFull.reduce(RooFit.Cut(label), RooFit.Name("dset"+str(i)), RooFit.Title("dset"+str(i))))

    res = pdfModel.fitTo(dset, RooFit.Save(True), RooFit.PrintLevel(0),RooFit.Extended())
    res = pdfModel.fitTo(dset, RooFit.Save(True), RooFit.PrintLevel(0),RooFit.Extended(), RooFit.Minos(True))

    EDM = res.edm()


    frame1 = Lst1_M.frame(RooFit.Title("#Lst_{1} mass"))
    dset.plotOn(frame1, RooFit.Binning(binNLst, minMassLst, maxMassLst))
    pdfModel.plotOn(frame1)

    frame2 = Lst2_M.frame(RooFit.Title("#Lst_{2} mass"))
    dset.plotOn(frame2, RooFit.Binning(binNLst, minMassLst, maxMassLst))
    pdfModel.plotOn(frame2)

    canvA = TCanvas("canvA", "canvA", 1000, 400) 
    canvA.Divide(2,1)
    canvA.cd(1)
    frame1.Draw()
    canvA.cd(2)
    frame2.Draw()
    canvA.SaveAs("binsFit/KpKpMass_"+str(i)+".pdf")

    shift = 0.5*(varNDiLst.getErrorHi()+varNDiLst.getErrorLo())
    err = 0.5*(varNDiLst.getErrorHi()-varNDiLst.getErrorLo())

    histLstLst.SetBinContent(i+1, varNDiLst.getVal()+shift)
    histLstLst.SetBinError(i+1, err)
                        


c = TCanvas("Jpsi2LstLst_Mass.pdf")
histLstLst.Draw()
histLstLst.SaveAs("Jpsi2LstLst_Mass.root")
c.SaveAs("Jpsi2LstLst_Mass.pdf")


















