from ROOT import *

chain  = TChain("DecayTree")
newfile = TFile("LstLst_RunII_merged.root","recreate")

jobIDs = [556,557,565,566]
for jobID in jobIDs:
   for i in range(0,600):
      filename = "/afs/cern.ch/work/a/ausachov/LstLstTuples/"+str(jobID)+"/"+str(i)+"/triggeredTupleAdd.root"
      chain.Add(filename)

tree = chain.CopyTree("")
tree.Print()
newfile.Write()
newfile.Close()









