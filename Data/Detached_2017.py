from ROOT import gROOT, TChain, TCut, TFile, TTree, TProof

#TProof.Open()
chain = TChain("Jpsi2LstLstDetachedTuple/DecayTree")
#chain.SetProof()

sjMax=400
for i in range(sjMax):
    chain.Add("/afs/cern.ch/work/a/ausachov/gangadir/workspace/ausachov/LocalXML/214/"+str(i)+"/output/Tuple.root")

for i in range(sjMax):
    chain.Add("/afs/cern.ch/work/a/ausachov/gangadir/workspace/ausachov/LocalXML/213/"+str(i)+"/output/Tuple.root")




#'TRCHI2DOF'        :    3.  ,
#    'CcbarPT'          :    3000,   #MeV
#        'KaonProbNNk'      :    0.1  ,
#        'KaonPT'           :    600. , # MeV
#        'KaonPTSec'        :    350. , # MeV
#        'ProtonProbNNp'    :    0.1  ,
#        'ProtonPT'         :    800.  , # MeV
#        'ProtonPTSec'      :    500.  , # MeV
#        'LstVtxChi2'       :    16.  ,
#        'LstMinMass'       :  1440  ,
#        'LstMaxMass'       :  1600  ,
#        'CombMaxMass'      :  6100.  , # MeV, before Vtx fit
#        'CombMinMass'      :  3000.  , # MeV, before Vtx fit
#        'MaxMass'          :  6000.  , # MeV, after Vtx fit
#        'MinMass'          :  2950.  , # MeV, after Vtx fit
#        'Lst_TisTosSpecs'  : { "Hlt1Global%TIS" : 0 }


cutID = TCut("ProtonP_ProbNNp>0.1 && ProtonM_ProbNNp>0.1 && KaonP_ProbNNk>0.1 && KaonM_ProbNNk>0.1")
cutTrack = TCut("ProtonP_TRACK_CHI2NDOF<3 && ProtonM_TRACK_CHI2NDOF<3 && KaonP_TRACK_CHI2NDOF<3 && KaonM_TRACK_CHI2NDOF<3")
cutProtonPT = TCut("ProtonP_PT>500 && ProtonM_PT>500")
cutKaonPT = TCut("KaonP_PT>300 && KaonM_PT>300")

cutLstVtx = TCut("Lst1_ENDVERTEX_CHI2<9 && Lst2_ENDVERTEX_CHI2<9")
cutLstM = TCut("Lst1_MM>1440 && Lst2_MM<1600")
cutJpsi = TCut("Jpsi_MM>2950 && Jpsi_MM<15100 && Jpsi_ENDVERTEX_CHI2<45 && Jpsi_FDCHI2_OWNPV>100")
cutL0 = TCut("Jpsi_L0HadronDecision_TOS || Jpsi_L0Global_TIS")
cutHLT1 = TCut("Jpsi_Hlt1TrackMVADecision_TOS || Jpsi_Hlt1TwoTrackMVADecision_TOS || Jpsi_Hlt1DiProtonDecision_TOS")
cutHLT2 = TCut("Jpsi_Hlt2Topo2BodyDecision_TOS || Jpsi_Hlt2Topo3BodyDecision_TOS || Jpsi_Hlt2Topo4BodyDecision_TOS || Jpsi_Hlt2CcDiHadronDiProtonDecision_TOS")






totCut = TCut(cutID+
              cutTrack+
              cutProtonPT+
              cutKaonPT+
              cutLstM+
              cutLstVtx+
              cutJpsi+
              cutL0+
              cutHLT1+
              cutHLT2)

newfile = TFile("Reduced_LstLstDetached_2016.root","recreate")

newtree=TTree()
newtree.SetMaxTreeSize(500000000)
newtree = chain.CopyTree(totCut.GetTitle())

newtree.Print()

newtree.GetCurrentFile().Write() 
newtree.GetCurrentFile().Close()

#newfile.Write()
