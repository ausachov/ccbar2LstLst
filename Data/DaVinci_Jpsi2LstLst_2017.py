def fillTuple( tuple, myTriggerList ):
        
    tuple.ToolList = ["TupleToolAngles",
                      "TupleToolEventInfo",
                      "TupleToolGeometry",
                      "TupleToolKinematic",
                      "TupleToolPid",
                      "TupleToolPrimaries",
                      "TupleToolTrackInfo",
                      "TupleToolRecoStats"
                     ]
    tuple.TupleToolRecoStats.Verbose=True

    from Configurables import TupleToolTISTOS, TupleToolDecay
    tuple.addTupleTool(TupleToolDecay, name = 'Jpsi')

    tuple.Jpsi.ToolList+=[ "TupleToolTISTOS/TupleToolTISTOSForJpsi" ]
    tuple.Jpsi.addTool(TupleToolTISTOS, name="TupleToolTISTOSForJpsi" )
    tuple.Jpsi.TupleToolTISTOSForJpsi.Verbose=True
    tuple.Jpsi.TupleToolTISTOSForJpsi.TriggerList = myTriggerList


    tuple.Jpsi.addTupleTool( 'TupleToolSubMass' )
    tuple.Jpsi.ToolList += [ "TupleToolSubMass" ]


    tuple.Jpsi.TupleToolSubMass.Substitution += ["K+ => pi+"]
    tuple.Jpsi.TupleToolSubMass.Substitution += ["K+ => p+"]
    tuple.Jpsi.TupleToolSubMass.Substitution += ["p+ => K+"]
    tuple.Jpsi.TupleToolSubMass.Substitution += ["p+ => pi+"]

    tuple.Jpsi.TupleToolSubMass.DoubleSubstitution += ["K-/p+ => p+/K-"]
    tuple.Jpsi.TupleToolSubMass.DoubleSubstitution += ["K-/p+ => pi+/K-"]
    tuple.Jpsi.TupleToolSubMass.DoubleSubstitution += ["K-/p+ => p+/pi-"]
    tuple.Jpsi.TupleToolSubMass.DoubleSubstitution += ["K-/p+ => pi+/pi-"]#

    tuple.Jpsi.TupleToolSubMass.DoubleSubstitution += ["K-/K+ => p+/pi-"]
    tuple.Jpsi.TupleToolSubMass.DoubleSubstitution += ["K-/K+ => pi+/pi-"]
    tuple.Jpsi.TupleToolSubMass.DoubleSubstitution += ["K-/K+ => p+/p~-"]
    tuple.Jpsi.TupleToolSubMass.DoubleSubstitution += ["K-/K+ => pi+/p~-"]#


    tuple.Jpsi.TupleToolSubMass.DoubleSubstitution += ["p~-/p+ => K-/pi+"]
    tuple.Jpsi.TupleToolSubMass.DoubleSubstitution += ["p~-/p+ => pi-/pi+"]
    tuple.Jpsi.TupleToolSubMass.DoubleSubstitution += ["p~-/p+ => K-/K+"]
    tuple.Jpsi.TupleToolSubMass.DoubleSubstitution += ["p~-/p+ => pi-/K+"]#

    tuple.Jpsi.TupleToolSubMass.Substitution += ["K+ => mu+"]
    tuple.Jpsi.TupleToolSubMass.Substitution += ["p+ => mu+"]
    tuple.Jpsi.TupleToolSubMass.Substitution += ["K+ => e+"]
    tuple.Jpsi.TupleToolSubMass.Substitution += ["p+ => e+"]


    # tuple.Jpsi.addTupleTool('TupleToolDecayTreeFitter/subsKpi')
    # tuple.Jpsi.subsKpi.Substitutions = {
    # 'Meson -> (Lambda(1520)0 -> p+ ^K-) Baryon': 'pi-',
    # 'Meson -> (Lambda(1520)~0 -> p~- ^K+) Baryon': 'pi+'
    # }



#    from Configurables import TupleToolDecayTreeFitter
#    tuple.Jpsi.ToolList +=  ["TupleToolDecayTreeFitter/PVFit"]        # fit with both PV and mass constraint
#    tuple.Jpsi.addTool(TupleToolDecayTreeFitter("PVFit"))
#    tuple.Jpsi.PVFit.Verbose = True
#    tuple.Jpsi.PVFit.constrainToOriginVertex = True
#    tuple.Jpsi.PVFit.daughtersToConstrain = []


    #LoKi one
    from Configurables import LoKi__Hybrid__TupleTool
    LoKi_Jpsi=LoKi__Hybrid__TupleTool("LoKi_Jpsi")
    LoKi_Jpsi.Variables = {
      "LOKI_FDCHI2"          : "BPVVDCHI2",
      "LOKI_FDS"             : "BPVDLS",
      "LOKI_DIRA"            : "BPVDIRA",
      "LOKI_BPVCORRM"        : "BPVCORRM",
      "m_scaled"             : "DTF_FUN ( M , False )",
      "DOCA"                 : "DOCA(1,2)",
      "m_pv"     : "DTF_FUN ( M , True )",
      "c2dtf_1"  : "DTF_CHI2NDOF( False )" ,
      "c2dtf_2"  : "DTF_CHI2NDOF( True  )"
    }
    tuple.Jpsi.addTupleTool(LoKi_Jpsi)


    tuple.addTool(TupleToolDecay, name = 'Lst1')
    tuple.Lst1.ToolList+=[ "TupleToolTISTOS/TupleToolTISTOSForLst1" ]
    tuple.Lst1.addTool(TupleToolTISTOS, name="TupleToolTISTOSForLst1" )
    tuple.Lst1.TupleToolTISTOSForLst1.Verbose=True
    tuple.Lst1.TupleToolTISTOSForLst1.TriggerList = myTriggerList

    from Configurables import LoKi__Hybrid__TupleTool
    LoKi_Lst1=LoKi__Hybrid__TupleTool("LoKi_Lst1")
    LoKi_Lst1.Variables = {
      "LOKI_FDCHI2"          : "BPVVDCHI2",
      "LOKI_FDS"             : "BPVDLS",
      "LOKI_DIRA"            : "BPVDIRA",
      "LOKI_BPVCORRM"        : "BPVCORRM",
      "m_scaled"           : "DTF_FUN ( M , False )",
      "DOCA"               : "DOCA(1,2)",
      "m_pv"     : "DTF_FUN ( M , True )",
      "c2dtf_1"  : "DTF_CHI2NDOF( False )",
      "c2dtf_2"  : "DTF_CHI2NDOF( True  )"
    }    
    tuple.Lst1.addTool(LoKi_Lst1)





    tuple.addTool(TupleToolDecay, name = 'Lst2')
    tuple.Lst2.ToolList+=[ "TupleToolTISTOS/TupleToolTISTOSForLst2" ]
    tuple.Lst2.addTool(TupleToolTISTOS, name="TupleToolTISTOSForLst2" )
    tuple.Lst2.TupleToolTISTOSForLst2.Verbose=True
    tuple.Lst2.TupleToolTISTOSForLst2.TriggerList = myTriggerList

    from Configurables import LoKi__Hybrid__TupleTool
    LoKi_Lst2=LoKi__Hybrid__TupleTool("LoKi_Lst2")
    LoKi_Lst2.Variables = {
      "LOKI_FDCHI2"          : "BPVVDCHI2",
      "LOKI_FDS"             : "BPVDLS",
      "LOKI_DIRA"            : "BPVDIRA",
      "LOKI_BPVCORRM"        : "BPVCORRM",
      "m_scaled"             : "DTF_FUN ( M , False )",
      "DOCA"                 : "DOCA(1,2)",
      "m_pv"     : "DTF_FUN ( M , True )",
      "c2dtf_1"  : "DTF_CHI2NDOF( False )",
      "c2dtf_2"  : "DTF_CHI2NDOF( True  )"
    }
    tuple.Lst2.addTool(LoKi_Lst2)



    #LoKi one
    from Configurables import LoKi__Hybrid__TupleTool
    LoKi_All=LoKi__Hybrid__TupleTool("LoKi_All")
    LoKi_All.Variables = {
        "ETA"                  : "ETA",
        "Y"                    : "Y"  ,
        "LOKI_IPCHI2"          : "BPVIPCHI2()"
        }
    tuple.addTool(LoKi_All)

    
myTriggerList = [
                 # L0
                 "L0ElectronDecision",
                 "L0PhotonDecision",
                 "L0HadronDecision",
                 # L0 Muon
                 "L0MuonDecision",
                 "L0MuonHighDecision",
                 "L0DiMuonDecision",
                 
                 
                 # Hlt1 track
                 "Hlt1B2PhiPhi_LTUNBDecision",
                 "Hlt1TrackMuonDecision",
                 "Hlt1TrackPhotonDecision",
                 
                 "Hlt1TrackAllL0Decision",
                 "Hlt1TrackMVADecision",
                 "Hlt1TrackMVALooseDecision",
                 "Hlt1TwoTrackMVADecision",
                 "Hlt1TwoTrackMVALooseDecision"
                 "Hlt1B2HH_LTUNB_KPiDecision",
                 "Hlt1B2HH_LTUNB_KKDecision",
                 "Hlt1B2HH_LTUNB_PiPiDecision",
                 "Hlt1IncPhiDecision",
                 "Hlt1DiProtonDecision",
                 "Hlt1CalibTrackingKKDecision",
                 "Hlt1DiProtonLowMultDecision",
                 "Hlt1LowMultVeloCut_HadronsDecision",
                 "Hlt1LowMultPassThroughDecision",
                
                 # Hlt2 Topo
                 "Hlt2PhiIncPhiDecision",
                 "Hlt2PhiPromptLst2EETurboDecision",
                 "Hlt2PhiBs2PhiPhiDecision",
                 "Hlt2B2HH_B2HHDecision",
                 "Hlt2CcDiHadronDiProtonDecision",
                 "Hlt2CcDiHadronDiProtonLowMultDecision",
                 "Hlt2CcDiHadronDiPhiDecision",
                 "Hlt2PhiIncPhiDecision",
                 "Hlt2PhiBs2PhiPhiDecision",
                 
                 "Hlt2Topo2BodyDecision",
                 "Hlt2Topo3BodyDecision",
                 "Hlt2Topo4BodyDecision"
    ]







year = "2017"
Jpsi2LstLstDecay = "J/psi(1S) -> ^( Lambda(1520)0 -> ^p+ ^K-) ^( Lambda(1520)~0  -> ^p~- ^K+)"
Jpsi2LstLstBranches = {
     "KaonM" :  "J/psi(1S) -> ( Lambda(1520)0 -> p+ ^K-) ( Lambda(1520)~0  -> p~- K+) "
    ,"KaonP" :  "J/psi(1S) -> ( Lambda(1520)0 -> p+ K-) ( Lambda(1520)~0  -> p~- ^K+) "
    ,"ProtonP" :  "J/psi(1S) -> ( Lambda(1520)0 -> ^p+ K-) ( Lambda(1520)~0  -> p~- K+) "
    ,"ProtonM" :  "J/psi(1S) -> ( Lambda(1520)0 -> p+ K-) ( Lambda(1520)~0  -> ^p~- K+) "
    ,"Lst1"  :  "J/psi(1S) -> ^( Lambda(1520)0 -> p+ K-) ( Lambda(1520)~0  -> p~- K+) "
    ,"Lst2"  :  "J/psi(1S) -> ( Lambda(1520)0 -> p+ K-) ^( Lambda(1520)~0  -> p~- K+) "
    ,"Jpsi"  :  "^(J/psi(1S) -> ( Lambda(1520)0 -> p+ K-) ( Lambda(1520)~0  -> p~- K+))"
    }

Jpsi2LstLstLocation = "Phys/Ccbar2LstLstLine/Particles"
from PhysConf.Selections import AutomaticData, MomentumScaling, TupleSelection
inputData = AutomaticData(Jpsi2LstLstLocation) 
inputData = MomentumScaling(inputData, Year = year)

Jpsi2LstLstTuple = TupleSelection("Jpsi2LstLstTuple", inputData, Decay = Jpsi2LstLstDecay, Branches = Jpsi2LstLstBranches)
fillTuple( Jpsi2LstLstTuple, myTriggerList )

from Configurables import EventNodeKiller
eventNodeKiller = EventNodeKiller('DAQkiller')
eventNodeKiller.Nodes = ['/Event/DAQ',
                         '/Event/pRec']


"""
Event-level filters
"""
from PhysConf.Filters import LoKi_Filters
Ccbar2LstLstFilters = LoKi_Filters (
    STRIP_Code = """HLT_PASS('StrippingCcbar2LstLstLineDecision')"""
    )



from Configurables import DaVinci, CondDB
CondDB ( LatestGlobalTagByDataType = year )
DaVinci().EventPreFilters = Ccbar2LstLstFilters.filters('Ccbar2LstLstFilters')
DaVinci().EvtMax = -1                         # Number of events
DaVinci().SkipEvents = 0                       # Events to skip
DaVinci().PrintFreq = 1000
DaVinci().DataType = year
DaVinci().Simulation    = False
DaVinci().HistogramFile = "DVHistos.root"      # Histogram file
DaVinci().TupleFile = "Tuple.root"             # Ntuple
DaVinci().UserAlgorithms = [ eventNodeKiller,
                            Jpsi2LstLstTuple ]        # The algorithms
# MDST
DaVinci().InputType = "MDST"
DaVinci().RootInTES = "/Event/Charm"

DaVinci().Lumi = True

DaVinci().DDDBtag   = "dddb-20170721-3"
#DaVinci().CondDBtag = "cond-20161004"



from Configurables import MessageSvc
MessageSvc().setWarning = [ 'RFileCnv' ]

#from GaudiConf import IOHelper
## Use the local input data
#IOHelper().inputFiles([
#                       '00069595_00000016_1.charm.mdst'
#                       ], clear=True)
