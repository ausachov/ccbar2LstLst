merged_filepath_prefix = '/eos/lhcb/wg/BandQ/PRODPOL/LstLst'
input_tree_nameLumi = 'GetIntegratedLuminosity/LumiTuple'

jobs = [133,134]
maxSj = 500

import os, sys, re
from ROOT import * 
from array import array
from math import sqrt


def caclLumi():

  tree = TTree()

  luminosity = array('d',[0])
  luminosityErr = array('d',[0])
  for iJ in jobs:
    intLumi = 0.
    intLumiErr = 0.
    jFolder = merged_filepath_prefix+"/"+str(iJ)+"/"
    for ii in range(maxSj):
      fileName = jFolder+str(ii)+"/Tuple.root"
      if os.path.isfile(fileName):
        try:
          file = TFile(fileName,"read")
          tree = file.Get("GetIntegratedLuminosity/LumiTuple")
          tree.SetBranchAddress("IntegratedLuminosity",luminosity)
          tree.SetBranchAddress("IntegratedLuminosityErr",luminosityErr)
          for iii in range(tree.GetEntries()):
            tree.GetEntry(iii)
            intLumi += luminosity[0]
            intLumiErr += luminosityErr[0]*luminosityErr[0]
        except:
          pass
      print ii, ' processed \n'
    intLumiErr = sqrt(intLumiErr)
    completeName = os.path.join(jFolder, 'lumi_'+str(iJ)+'.txt')
    file = open(completeName,'w') 
    file.write("IntegratedLuminosity is  {} \n".format(intLumi))
    file.write("IntegratedLuminosityErr is  {}".format(intLumiErr))
    file.close()


caclLumi()