import os
from ROOT import *

jobsN = [134]
nSubJobs = 500


reducingCut = "(Jpsi_Hlt1TrackMVADecision_TOS || Jpsi_Hlt1TwoTrackMVADecision_TOS) && Lst1_ENDVERTEX_CHI2<9 && Lst2_ENDVERTEX_CHI2<9 && Jpsi_ENDVERTEX_CHI2<45 && KaonP_PT>250 && KaonM_PT>250 && ProtonP_PT>500 && ProtonM_PT>500 && ProtonP_ProbNNp>0.2 && ProtonM_ProbNNp>0.2 && KaonP_ProbNNk>0.1 && KaonM_ProbNNk>0.1"


for iJ in jobsN:
   mainDir = '/eos/lhcb/wg/BandQ/PRODPOL/LstLst/'+str(iJ)
   for nSj in range(nSubJobs):
      sjDir = mainDir+'/'+str(nSj)
      try:
         chainFileName = sjDir+'/Tuple.root'
         ch = TChain("Jpsi2LstLstTuple/DecayTree")
         ch.Add(chainFileName)
         newfile = TFile(sjDir+"/reducedTuple.root","recreate")
         tree = ch.CopyTree(reducingCut)
         tree.GetCurrentFile().Write() 
         tree.GetCurrentFile().Close()
      except:
         pass
      print nSj, "processed"

