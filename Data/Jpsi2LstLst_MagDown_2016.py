myJobName = 'Jpsi2LstLst_R16S28r1p1_MagDown_2016'

myApplication = GaudiExec()
myApplication.directory = "$HOME/cmtuser/DaVinciDev_v42r6p1"
myApplication.options = ['DaVinci_Jpsi2LstLst_2016.py']

data  = BKQuery('/LHCb/Collision16/Beam6500GeV-VeloClosed-MagDown/Real Data/Reco16/Stripping28r1p1/90000000/CHARM.MDST', dqflag=['OK']).getDataset()

validData = LHCbDataset(files=['LFN:'+lfn for lfn, rep in data.getReplicas().iteritems() if len(rep)])

mySplitter = SplitByFiles( filesPerJob = 10, maxFiles = -1, ignoremissing = True, bulksubmit = False )

myBackend = Dirac()
j = Job (
         name         = myJobName,
         application  = myApplication,
         splitter     = mySplitter,
         outputfiles  = [ DiracFile('Tuple.root'),
                         DiracFile('DVHistos.root')
                         ],
         backend      = myBackend,
         inputdata    = validData,
         do_auto_resubmit = True,
         parallel_submit = True
         )
j.submit(keep_going=True, keep_on_fail=True)

