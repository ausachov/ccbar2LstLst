from ROOT import *
from ROOT.TMath import Sqrt, Sq
from array import array

mPi = 139.57018
mK  = 493.67700
mP  = 938.27205 



def fillNewTuple(filename,newfilename):

   chain  = TChain("DecayTree")
   chain.Add(filename)
   newfile = TFile(newfilename,"recreate")

   cutTrigger = "(Jpsi_L0HadronDecision_TOS || Jpsi_L0Global_TIS)"
   cutTrack = "ProtonP_TRACK_CHI2NDOF<3 && ProtonM_TRACK_CHI2NDOF<3 && KaonP_TRACK_CHI2NDOF<3 && KaonM_TRACK_CHI2NDOF<3"
   cutVtx = "Lst1_ENDVERTEX_CHI2<9 && Lst2_ENDVERTEX_CHI2<9 && Jpsi_ENDVERTEX_CHI2<45"
   cutPID = "ProtonP_ProbNNp>0.4 && ProtonM_ProbNNp>0.4 && KaonP_ProbNNk>0.1 && KaonM_ProbNNk>0.1"
   cutP = "ProtonP_P>10000 && ProtonM_P>10000"
   cutDef = cutTrigger+" && "+cutTrack+" && "+cutVtx+" && "+cutPID+" && "+cutP

   newtree = chain.CopyTree(cutDef)


   KaonM_P  = array('d',[0]); newtree.SetBranchAddress("KaonM_P", KaonM_P)
   KaonM_PE = array('d',[0]); newtree.SetBranchAddress("KaonM_PE",KaonM_PE)
   KaonM_PX = array('d',[0]); newtree.SetBranchAddress("KaonM_PX",KaonM_PX)
   KaonM_PY = array('d',[0]); newtree.SetBranchAddress("KaonM_PY",KaonM_PY)
   KaonM_PZ = array('d',[0]); newtree.SetBranchAddress("KaonM_PZ",KaonM_PZ)
   KaonM_PT = array('d',[0]); newtree.SetBranchAddress("KaonM_PT",KaonM_PT)

   KaonP_P  = array('d',[0]); newtree.SetBranchAddress("KaonP_P",KaonP_P)
   KaonP_PE = array('d',[0]); newtree.SetBranchAddress("KaonP_PE",KaonP_PE)
   KaonP_PX = array('d',[0]); newtree.SetBranchAddress("KaonP_PX",KaonP_PX)
   KaonP_PY = array('d',[0]); newtree.SetBranchAddress("KaonP_PY",KaonP_PY)
   KaonP_PZ = array('d',[0]); newtree.SetBranchAddress("KaonP_PZ",KaonP_PZ)
   KaonP_PT = array('d',[0]); newtree.SetBranchAddress("KaonP_PT",KaonP_PT)

   ProtonM_P  = array('d',[0]); newtree.SetBranchAddress("ProtonM_P",ProtonM_P)
   ProtonM_PE = array('d',[0]); newtree.SetBranchAddress("ProtonM_PE",ProtonM_PE)
   ProtonM_PX = array('d',[0]); newtree.SetBranchAddress("ProtonM_PX",ProtonM_PX)
   ProtonM_PY = array('d',[0]); newtree.SetBranchAddress("ProtonM_PY",ProtonM_PY)
   ProtonM_PZ = array('d',[0]); newtree.SetBranchAddress("ProtonM_PZ",ProtonM_PZ)
   ProtonM_PT = array('d',[0]); newtree.SetBranchAddress("ProtonM_PT",ProtonM_PT)

   ProtonP_P  = array('d',[0]); newtree.SetBranchAddress("ProtonP_P",ProtonP_P)
   ProtonP_PE = array('d',[0]); newtree.SetBranchAddress("ProtonP_PE",ProtonP_PE)
   ProtonP_PX = array('d',[0]); newtree.SetBranchAddress("ProtonP_PX",ProtonP_PX)
   ProtonP_PY = array('d',[0]); newtree.SetBranchAddress("ProtonP_PY",ProtonP_PY)
   ProtonP_PZ = array('d',[0]); newtree.SetBranchAddress("ProtonP_PZ",ProtonP_PZ)
   ProtonP_PT = array('d',[0]); newtree.SetBranchAddress("ProtonP_PT",ProtonP_PT)

   Lst1_P  = array('d',[0]); newtree.SetBranchAddress("Lst1_P",Lst1_P)
   Lst1_PE = array('d',[0]); newtree.SetBranchAddress("Lst1_PE",Lst1_PE)
   Lst1_PX = array('d',[0]); newtree.SetBranchAddress("Lst1_PX",Lst1_PX)
   Lst1_PY = array('d',[0]); newtree.SetBranchAddress("Lst1_PY",Lst1_PY)
   Lst1_PZ = array('d',[0]); newtree.SetBranchAddress("Lst1_PZ",Lst1_PZ)
   Lst1_PT = array('d',[0]); newtree.SetBranchAddress("Lst1_PT",Lst1_PT)

   Lst2_P  = array('d',[0]); newtree.SetBranchAddress("Lst2_P",Lst2_P)
   Lst2_PE = array('d',[0]); newtree.SetBranchAddress("Lst2_PE",Lst2_PE)
   Lst2_PX = array('d',[0]); newtree.SetBranchAddress("Lst2_PX",Lst2_PX)
   Lst2_PY = array('d',[0]); newtree.SetBranchAddress("Lst2_PY",Lst2_PY)
   Lst2_PZ = array('d',[0]); newtree.SetBranchAddress("Lst2_PZ",Lst2_PZ)
   Lst2_PT = array('d',[0]); newtree.SetBranchAddress("Lst2_PT",Lst2_PT)


   Phi_M  = array('d',[0]); Phi_M_Branch  = newtree.Branch("Phi_M", Phi_M, "Phi_M/D")
   Phi_P  = array('d',[0]); Phi_P_Branch  = newtree.Branch("Phi_P", Phi_P, "Phi_P/D")
   Phi_PE = array('d',[0]); Phi_PE_Branch = newtree.Branch("Phi_PE",Phi_PE,"Phi_PE/D")


   Etac_M  = array('d',[0]); Etac_M_Branch  = newtree.Branch("Etac_M", Etac_M, "Etac_M/D")
   Etac_P  = array('d',[0]); Etac_P_Branch  = newtree.Branch("Etac_P", Etac_P, "Etac_P/D")
   Etac_PE = array('d',[0]); Etac_PE_Branch = newtree.Branch("Etac_PE",Etac_PE,"Etac_PE/D")


   Lst1ws_M  = array('d',[0]); Lst1ws_M_Branch  = newtree.Branch("Lst1ws_M", Lst1ws_M, "Lst1ws_M/D")
   Lst1ws_P  = array('d',[0]); Lst1ws_P_Branch  = newtree.Branch("Lst1ws_P", Lst1ws_P, "Lst1ws_P/D")
   Lst1ws_PE = array('d',[0]); Lst1ws_PE_Branch = newtree.Branch("Lst1ws_PE",Lst1ws_PE,"Lst1ws_PE/D")



   Lst2ws_M  = array('d',[0]); Lst2ws_M_Branch  = newtree.Branch("Lst2ws_M", Lst2ws_M, "Lst2ws_M/D")
   Lst2ws_P  = array('d',[0]); Lst2ws_P_Branch  = newtree.Branch("Lst2ws_P", Lst2ws_P, "Lst2ws_P/D")
   Lst2ws_PE = array('d',[0]); Lst2ws_PE_Branch = newtree.Branch("Lst2ws_PE",Lst2ws_PE,"Lst2ws_PE/D")


   Lst1_K2pi_M  = array('d',[0]); Lst1_K2pi_M_Branch  = newtree.Branch("Lst1_K2pi_M", Lst1_K2pi_M, "Lst1_K2pi_M/D")
   Lst1_K2pi_P  = array('d',[0]); Lst1_K2pi_P_Branch  = newtree.Branch("Lst1_K2pi_P", Lst1_K2pi_P, "Lst1_K2pi_P/D")
   Lst1_K2pi_PE = array('d',[0]); Lst1_K2pi_PE_Branch = newtree.Branch("Lst1_K2pi_PE",Lst1_K2pi_PE,"Lst1_K2pi_PE/D")

   Lst2_K2pi_M  = array('d',[0]); Lst2_K2pi_M_Branch  = newtree.Branch("Lst2_K2pi_M", Lst2_K2pi_M, "Lst2_K2pi_M/D")
   Lst2_K2pi_P  = array('d',[0]); Lst2_K2pi_P_Branch  = newtree.Branch("Lst2_K2pi_P", Lst2_K2pi_P, "Lst2_K2pi_P/D")
   Lst2_K2pi_PE = array('d',[0]); Lst2_K2pi_PE_Branch = newtree.Branch("Lst2_K2pi_PE",Lst2_K2pi_PE,"Lst2_K2pi_PE/D")



   Lst1_K2p_M  = array('d',[0]); Lst1_K2p_M_Branch  = newtree.Branch("Lst1_K2p_M", Lst1_K2p_M, "Lst1_K2p_M/D")
   Lst1_K2p_P  = array('d',[0]); Lst1_K2p_P_Branch  = newtree.Branch("Lst1_K2p_P", Lst1_K2p_P, "Lst1_K2p_P/D")
   Lst1_K2p_PE = array('d',[0]); Lst1_K2p_PE_Branch = newtree.Branch("Lst1_K2p_PE",Lst1_K2p_PE,"Lst1_K2p_PE/D")




   Lst2_K2p_M  = array('d',[0]); Lst2_K2p_M_Branch  = newtree.Branch("Lst2_K2p_M", Lst2_K2p_M, "Lst2_K2p_M/D")
   Lst2_K2p_P  = array('d',[0]); Lst2_K2p_P_Branch  = newtree.Branch("Lst2_K2p_P", Lst2_K2p_P, "Lst2_K2p_P/D")
   Lst2_K2p_PE = array('d',[0]); Lst2_K2p_PE_Branch = newtree.Branch("Lst2_K2p_PE",Lst2_K2p_PE,"Lst2_K2p_PE/D")




   Lst1_p2pi_M  = array('d',[0]); Lst1_p2pi_M_Branch  = newtree.Branch("Lst1_p2pi_M", Lst1_p2pi_M, "Lst1_p2pi_M/D")
   Lst1_p2pi_P  = array('d',[0]); Lst1_p2pi_P_Branch  = newtree.Branch("Lst1_p2pi_P", Lst1_p2pi_P, "Lst1_p2pi_P/D")
   Lst1_p2pi_PE = array('d',[0]); Lst1_p2pi_PE_Branch = newtree.Branch("Lst1_p2pi_PE",Lst1_p2pi_PE,"Lst1_p2pi_PE/D")



   Lst2_p2pi_M  = array('d',[0]); Lst2_p2pi_M_Branch  = newtree.Branch("Lst2_p2pi_M", Lst2_p2pi_M, "Lst2_p2pi_M/D")
   Lst2_p2pi_P  = array('d',[0]); Lst2_p2pi_P_Branch  = newtree.Branch("Lst2_p2pi_P", Lst2_p2pi_P, "Lst2_p2pi_P/D")
   Lst2_p2pi_PE = array('d',[0]); Lst2_p2pi_PE_Branch = newtree.Branch("Lst2_p2pi_PE",Lst2_p2pi_PE,"Lst2_p2pi_PE/D")


   Lst1_p2K_M  = array('d',[0]); Lst1_p2K_M_Branch  = newtree.Branch("Lst1_p2K_M", Lst1_p2K_M, "Lst1_p2K_M/D")
   Lst1_p2K_P  = array('d',[0]); Lst1_p2K_P_Branch  = newtree.Branch("Lst1_p2K_P", Lst1_p2K_P, "Lst1_p2K_P/D")
   Lst1_p2K_PE = array('d',[0]); Lst1_p2K_PE_Branch = newtree.Branch("Lst1_p2K_PE",Lst1_p2K_PE,"Lst1_p2K_PE/D")



   Lst2_p2K_M  = array('d',[0]); Lst2_p2K_M_Branch  = newtree.Branch("Lst2_p2K_M", Lst2_p2K_M, "Lst2_p2K_M/D")
   Lst2_p2K_P  = array('d',[0]); Lst2_p2K_P_Branch  = newtree.Branch("Lst2_p2K_P", Lst2_p2K_P, "Lst2_p2K_P/D")
   Lst2_p2K_PE = array('d',[0]); Lst2_p2K_PE_Branch = newtree.Branch("Lst2_p2K_PE",Lst2_p2K_PE,"Lst2_p2K_PE/D")



   Phi_Km2Pim_M  = array('d',[0]); Phi_Km2Pim_M_Branch  = newtree.Branch("Phi_Km2Pim_M", Phi_Km2Pim_M, "Phi_Km2Pim_M/D")
   Phi_Km2Pim_P  = array('d',[0]); Phi_Km2Pim_P_Branch  = newtree.Branch("Phi_Km2Pim_P", Phi_Km2Pim_P, "Phi_Km2Pim_P/D")
   Phi_Km2Pim_PE = array('d',[0]); Phi_Km2Pim_PE_Branch = newtree.Branch("Phi_Km2Pim_PE",Phi_Km2Pim_PE,"Phi_Km2Pim_PE/D")

   Phi_Kp2Pip_M  = array('d',[0]); Phi_Kp2Pip_M_Branch  = newtree.Branch("Phi_Kp2Pip_M", Phi_Kp2Pip_M, "Phi_Kp2Pip_M/D")
   Phi_Kp2Pip_P  = array('d',[0]); Phi_Kp2Pip_P_Branch  = newtree.Branch("Phi_Kp2Pip_P", Phi_Kp2Pip_P, "Phi_Kp2Pip_P/D")
   Phi_Kp2Pip_PE = array('d',[0]); Phi_Kp2Pip_PE_Branch = newtree.Branch("Phi_Kp2Pip_PE",Phi_Kp2Pip_PE,"Phi_Kp2Pip_PE/D")




   Etac_Pm2Pim_M  = array('d',[0]); Etac_Pm2Pim_M_Branch  = newtree.Branch("Etac_Pm2Pim_M", Etac_Pm2Pim_M, "Etac_Pm2Pim_M/D")
   Etac_Pm2Pim_P  = array('d',[0]); Etac_Pm2Pim_P_Branch  = newtree.Branch("Etac_Pm2Pim_P", Etac_Pm2Pim_P, "Etac_Pm2Pim_P/D")
   Etac_Pm2Pim_PE = array('d',[0]); Etac_Pm2Pim_PE_Branch = newtree.Branch("Etac_Pm2Pim_PE",Etac_Pm2Pim_PE,"Etac_Pm2Pim_PE/D")

   Etac_Pp2Pip_M  = array('d',[0]); Etac_Pp2Pip_M_Branch  = newtree.Branch("Etac_Pp2Pip_M", Etac_Pp2Pip_M, "Etac_Pp2Pip_M/D")
   Etac_Pp2Pip_P  = array('d',[0]); Etac_Pp2Pip_P_Branch  = newtree.Branch("Etac_Pp2Pip_P", Etac_Pp2Pip_P, "Etac_Pp2Pip_P/D")
   Etac_Pp2Pip_PE = array('d',[0]); Etac_Pp2Pip_PE_Branch = newtree.Branch("Etac_Pp2Pip_PE",Etac_Pp2Pip_PE,"Etac_Pp2Pip_PE/D")




# 3 body
   PpKK_M  = array('d',[0]); PpKK_M_Branch  = newtree.Branch("PpKK_M", PpKK_M, "PpKK_M/D")
   PpKK_P  = array('d',[0]); PpKK_P_Branch  = newtree.Branch("PpKK_P", PpKK_P, "PpKK_P/D")
   PpKK_PE = array('d',[0]); PpKK_PE_Branch = newtree.Branch("PpKK_PE",PpKK_PE,"PpKK_PE/D")

   PmKK_M  = array('d',[0]); PmKK_M_Branch  = newtree.Branch("PmKK_M", PmKK_M, "PmKK_M/D")
   PmKK_P  = array('d',[0]); PmKK_P_Branch  = newtree.Branch("PmKK_P", PmKK_P, "PmKK_P/D")
   PmKK_PE = array('d',[0]); PmKK_PE_Branch = newtree.Branch("PmKK_PE",PmKK_PE,"PmKK_PE/D")





   PpKm_Kp2Pip_M  = array('d',[0]); PpKm_Kp2Pip_M_Branch  = newtree.Branch("PpKm_Kp2Pip_M", PpKm_Kp2Pip_M, "PpKm_Kp2Pip_M/D")
   PpKm_Kp2Pip_P  = array('d',[0]); PpKm_Kp2Pip_P_Branch  = newtree.Branch("PpKm_Kp2Pip_P", PpKm_Kp2Pip_P, "PpKm_Kp2Pip_P/D")
   PpKm_Kp2Pip_PE = array('d',[0]); PpKm_Kp2Pip_PE_Branch = newtree.Branch("PpKm_Kp2Pip_PE",PpKm_Kp2Pip_PE,"PpKm_Kp2Pip_PE/D")




   PmKp_Km2Pim_M  = array('d',[0]); PmKp_Km2Pim_M_Branch  = newtree.Branch("PmKp_Km2Pim_M", PmKp_Km2Pim_M, "PmKp_Km2Pim_M/D")
   PmKp_Km2Pim_P  = array('d',[0]); PmKp_Km2Pim_P_Branch  = newtree.Branch("PmKp_Km2Pim_P", PmKp_Km2Pim_P, "PmKp_Km2Pim_P/D")
   PmKp_Km2Pim_PE = array('d',[0]); PmKp_Km2Pim_PE_Branch = newtree.Branch("PmKp_Km2Pim_PE",PmKp_Km2Pim_PE,"PmKp_Km2Pim_PE/D")


   Phi_Km2Pm_M  = array('d',[0]); Phi_Km2Pm_M_Branch  = newtree.Branch("Phi_Km2Pm_M", Phi_Km2Pm_M, "Phi_Km2Pm_M/D")
   Phi_Km2Pm_P  = array('d',[0]); Phi_Km2Pm_P_Branch  = newtree.Branch("Phi_Km2Pm_P", Phi_Km2Pm_P, "Phi_Km2Pm_P/D")
   Phi_Km2Pm_PE = array('d',[0]); Phi_Km2Pm_PE_Branch = newtree.Branch("Phi_Km2Pm_PE",Phi_Km2Pm_PE,"Phi_Km2Pm_PE/D")


   Phi_Kp2Pp_M  = array('d',[0]); Phi_Kp2Pp_M_Branch  = newtree.Branch("Phi_Kp2Pp_M", Phi_Kp2Pp_M, "Phi_Kp2Pp_M/D")
   Phi_Kp2Pp_P  = array('d',[0]); Phi_Kp2Pp_P_Branch  = newtree.Branch("Phi_Kp2Pp_P", Phi_Kp2Pp_P, "Phi_Kp2Pp_P/D")
   Phi_Kp2Pp_PE = array('d',[0]); Phi_Kp2Pp_PE_Branch = newtree.Branch("Phi_Kp2Pp_PE",Phi_Kp2Pp_PE,"Phi_Kp2Pp_PE/D")



   Etac_Pm2Km_M  = array('d',[0]); Etac_Pm2Km_M_Branch  = newtree.Branch("Etac_Pm2Km_M", Etac_Pm2Km_M, "Etac_Pm2Km_M/D")
   Etac_Pm2Km_P  = array('d',[0]); Etac_Pm2Km_P_Branch  = newtree.Branch("Etac_Pm2Km_P", Etac_Pm2Km_P, "Etac_Pm2Km_P/D")
   Etac_Pm2Km_PE = array('d',[0]); Etac_Pm2Km_PE_Branch = newtree.Branch("Etac_Pm2Km_PE",Etac_Pm2Km_PE,"Etac_Pm2Km_PE/D")


   Etac_Pp2Kp_M  = array('d',[0]); Etac_Pp2Kp_M_Branch  = newtree.Branch("Etac_Pp2Kp_M", Etac_Pp2Kp_M, "Etac_Pp2Kp_M/D")
   Etac_Pp2Kp_P  = array('d',[0]); Etac_Pp2Kp_P_Branch  = newtree.Branch("Etac_Pp2Kp_P", Etac_Pp2Kp_P, "Etac_Pp2Kp_P/D")
   Etac_Pp2Kp_PE = array('d',[0]); Etac_Pp2Kp_PE_Branch = newtree.Branch("Etac_Pp2Kp_PE",Etac_Pp2Kp_PE,"Etac_Pp2Kp_PE/D")



   Etac_PpPm2KpKm_M  = array('d',[0]); Etac_PpPm2KpKm_M_Branch  = newtree.Branch("Etac_PpPm2KpKm_M", Etac_PpPm2KpKm_M, "Etac_PpPm2KpKm_M/D")
   Etac_PpPm2KpKm_P  = array('d',[0]); Etac_PpPm2KpKm_P_Branch  = newtree.Branch("Etac_PpPm2KpKm_P", Etac_PpPm2KpKm_P, "Etac_PpPm2KpKm_P/D")
   Etac_PpPm2KpKm_PE = array('d',[0]); Etac_PpPm2KpKm_PE_Branch = newtree.Branch("Etac_PpPm2KpKm_PE",Etac_PpPm2KpKm_PE,"Etac_PpPm2KpKm_PE/D")


   PPKm_M  = array('d',[0]); PPKm_M_Branch  = newtree.Branch("PPKm_M", PPKm_M, "PPKm_M/D")
   PPKm_P  = array('d',[0]); PPKm_P_Branch  = newtree.Branch("PPKm_P", PPKm_P, "PPKm_P/D")
   PPKm_PE = array('d',[0]); PPKm_PE_Branch = newtree.Branch("PPKm_PE",PPKm_PE,"PPKm_PE/D")

   PmKm_Pp2Pip_M  = array('d',[0]); PmKm_Pp2Pip_M_Branch  = newtree.Branch("PmKm_Pp2Pip_M", PmKm_Pp2Pip_M, "PmKm_Pp2Pip_M/D")
   PmKm_Pp2Pip_P  = array('d',[0]); PmKm_Pp2Pip_P_Branch  = newtree.Branch("PmKm_Pp2Pip_P", PmKm_Pp2Pip_P, "PmKm_Pp2Pip_P/D")
   PmKm_Pp2Pip_PE = array('d',[0]); PmKm_Pp2Pip_PE_Branch = newtree.Branch("PmKm_Pp2Pip_PE",PmKm_Pp2Pip_PE,"PmKm_Pp2Pip_PE/D")


   PPKp_M  = array('d',[0]); PPKp_M_Branch  = newtree.Branch("PPKp_M", PPKp_M, "PPKp_M/D")
   PPKp_P  = array('d',[0]); PPKp_P_Branch  = newtree.Branch("PPKp_P", PPKp_P, "PPKp_P/D")
   PPKp_PE = array('d',[0]); PPKp_PE_Branch = newtree.Branch("PPKp_PE",PPKp_PE,"PPKp_PE/D")


   PpKp_Pm2Pim_M  = array('d',[0]); PpKp_Pm2Pim_M_Branch  = newtree.Branch("PpKp_Pm2Pim_M", PpKp_Pm2Pim_M, "PpKp_Pm2Pim_M/D")
   PpKp_Pm2Pim_P  = array('d',[0]); PpKp_Pm2Pim_P_Branch  = newtree.Branch("PpKp_Pm2Pim_P", PpKp_Pm2Pim_P, "PpKp_Pm2Pim_P/D")
   PpKp_Pm2Pim_PE = array('d',[0]); PpKp_Pm2Pim_PE_Branch = newtree.Branch("PpKp_Pm2Pim_PE",PpKp_Pm2Pim_PE,"PpKp_Pm2Pim_PE/D")


   nEn = newtree.GetEntries()
   for i in range(nEn):
      newtree.GetEntry(i)
      Phi_PE[0] = KaonM_PE[0] + KaonP_PE[0]
      Phi_P[0]  = Sqrt((KaonM_PX[0] + KaonP_PX[0])**2 + \
                       (KaonM_PY[0] + KaonP_PY[0])**2 + \
                       (KaonM_PZ[0] + KaonP_PZ[0])**2)
      valM2 = Phi_PE[0]**2-Phi_P[0]**2
      if(valM2>0):
         Phi_M[0] = Sqrt(valM2)
      else:
         Phi_M[0] = -999


      Etac_PE[0] = ProtonM_PE[0] + ProtonP_PE[0]
      Etac_P[0]  = Sqrt((ProtonM_PX[0] + ProtonP_PX[0])**2 + \
                        (ProtonM_PY[0] + ProtonP_PY[0])**2 + \
                        (ProtonM_PZ[0] + ProtonP_PZ[0])**2)
      valM2 = Etac_PE[0]**2-Etac_P[0]**2
      if(valM2>0):
         Etac_M[0] = Sqrt(valM2)
      else:
         Etac_M[0] = -999


      Lst1ws_PE[0] = KaonM_PE[0] + ProtonM_PE[0]
      Lst1ws_P[0] = Sqrt((KaonM_PX[0] + ProtonM_PX[0])**2 + \
                         (KaonM_PY[0] + ProtonM_PY[0])**2 + \
                         (KaonM_PZ[0] + ProtonM_PZ[0])**2)
      valM2 = Lst1ws_PE[0]**2-Lst1ws_P[0]**2
      if(valM2>0):
         Lst1ws_M[0] = Sqrt(valM2)
      else:
         Lst1ws_M[0] = -999


      Lst2ws_PE[0] = KaonP_PE[0] + ProtonP_PE[0]
      Lst2ws_P[0]  = Sqrt((KaonP_PX[0] + ProtonP_PX[0])**2 + \
                          (KaonP_PY[0] + ProtonP_PY[0])**2 + \
                          (KaonP_PZ[0] + ProtonP_PZ[0])**2)
      valM2 = Lst2ws_PE[0]**2-Lst2ws_P[0]**2
      if(valM2>0):
         Lst2ws_M[0] = Sqrt(valM2)
      else:
         Lst2ws_M[0] = -999

      # Lst1:Lambda(1520)0 -> p+ K-
      Lst1_K2pi_PE[0] = ProtonP_PE[0] + Sqrt(KaonM_P[0]**2 + mPi**2)
      Lst1_K2pi_P[0]  = Sqrt((ProtonP_PX[0] + KaonM_PX[0])**2 + \
                             (ProtonP_PY[0] + KaonM_PY[0])**2 + \
                             (ProtonP_PZ[0] + KaonM_PZ[0])**2)
      valM2 = Lst1_K2pi_PE[0]**2-Lst1_K2pi_P[0]**2
      if(valM2>0):
         Lst1_K2pi_M[0] = Sqrt(valM2)
      else:
         Lst1_K2pi_M[0] = -999

      Lst1_K2p_PE[0] = ProtonP_PE[0] + Sqrt(KaonM_P[0]**2 + mP**2)
      Lst1_K2p_P[0]  = Sqrt((ProtonP_PX[0] + KaonM_PX[0])**2 + \
                            (ProtonP_PY[0] + KaonM_PY[0])**2 + \
                            (ProtonP_PZ[0] + KaonM_PZ[0])**2)
      valM2 = Lst1_K2p_PE[0]**2-Lst1_K2p_P[0]**2
      if(valM2>0):
         Lst1_K2p_M[0] = Sqrt(valM2)
      else:
         Lst1_K2p_M[0] = -999


      Lst1_p2pi_PE[0] = KaonM_PE[0] + Sqrt(ProtonP_P[0]**2 + mPi**2)
      Lst1_p2pi_P[0]  = Sqrt((ProtonP_PX[0] + KaonM_PX[0])**2 + \
                             (ProtonP_PY[0] + KaonM_PY[0])**2 + \
                             (ProtonP_PZ[0] + KaonM_PZ[0])**2)
      valM2 = Lst1_p2pi_PE[0]**2-Lst1_p2pi_P[0]**2
      if(valM2>0):
         Lst1_p2pi_M[0] = Sqrt(valM2)
      else:
         Lst1_p2pi_M[0] = -999

      Lst1_p2K_PE[0] = KaonM_PE[0] + Sqrt(ProtonP_P[0]**2 + mK**2)
      Lst1_p2K_P[0]  = Sqrt((ProtonP_PX[0] + KaonM_PX[0])**2 + \
                            (ProtonP_PY[0] + KaonM_PY[0])**2 + \
                            (ProtonP_PZ[0] + KaonM_PZ[0])**2)
      valM2 = Lst1_p2K_PE[0]**2-Lst1_p2K_P[0]**2
      if(valM2>0):
         Lst1_p2K_M[0] = Sqrt(valM2)
      else:
         Lst1_p2K_M[0] = -999


      # Lst2:Lambda(1520)~0 -> p~- K+
      Lst2_K2pi_PE[0] = ProtonM_PE[0] + Sqrt(KaonP_P[0]**2 + mPi**2)
      Lst2_K2pi_P[0]  = Sqrt((ProtonM_PX[0] + KaonP_PX[0])**2 + \
                             (ProtonM_PY[0] + KaonP_PY[0])**2 + \
                             (ProtonM_PZ[0] + KaonP_PZ[0])**2)
      valM2 = Lst2_K2pi_PE[0]**2-Lst2_K2pi_P[0]**2
      if(valM2>0):
         Lst2_K2pi_M[0] = Sqrt(valM2)
      else:
         Lst2_K2pi_M[0] = -999

      Lst2_K2p_PE[0] = ProtonM_PE[0] + Sqrt(KaonP_P[0]**2 + mP**2)
      Lst2_K2p_P[0]  = Sqrt((ProtonM_PX[0] + KaonP_PX[0])**2 + \
                            (ProtonM_PY[0] + KaonP_PY[0])**2 + \
                            (ProtonM_PZ[0] + KaonP_PZ[0])**2)
      valM2 = Lst2_K2p_PE[0]**2-Lst2_K2p_P[0]**2
      if(valM2>0):
         Lst2_K2p_M[0] = Sqrt(valM2)
      else:
         Lst2_K2p_M[0] = -999


      Lst2_p2pi_PE[0] = KaonP_PE[0] + Sqrt(ProtonM_P[0]**2 + mPi**2)
      Lst2_p2pi_P[0]  = Sqrt((ProtonM_PX[0] + KaonP_PX[0])**2 + \
                             (ProtonM_PY[0] + KaonP_PY[0])**2 + \
                             (ProtonM_PZ[0] + KaonP_PZ[0])**2)
      valM2 = Lst2_p2pi_PE[0]**2-Lst2_p2pi_P[0]**2
      if(valM2>0):
         Lst2_p2pi_M[0] = Sqrt(valM2)
      else:
         Lst2_p2pi_M[0] = -999

      Lst2_p2K_PE[0] = KaonP_PE[0] + Sqrt(ProtonM_P[0]**2 + mK**2)
      Lst2_p2K_P[0]  = Sqrt((ProtonM_PX[0] + KaonP_PX[0])**2 + \
                            (ProtonM_PY[0] + KaonP_PY[0])**2 + \
                            (ProtonM_PZ[0] + KaonP_PZ[0])**2)
      valM2 = Lst2_p2K_PE[0]**2-Lst2_p2K_P[0]**2
      if(valM2>0):
         Lst2_p2K_M[0] = Sqrt(valM2)
      else:
         Lst2_p2K_M[0] = -999


      #Kp Km->Pim
      Phi_Km2Pim_PE[0] = KaonP_PE[0] + Sqrt(KaonM_P[0]**2 + mPi**2)
      Phi_Km2Pim_P[0]  = Phi_P[0]
      valM2 = Phi_Km2Pim_PE[0]**2-Phi_Km2Pim_P[0]**2
      if(valM2>0):
         Phi_Km2Pim_M[0] = Sqrt(valM2)
      else:
         Phi_Km2Pim_M[0] = -999


      #Kp Km->Pm
      Phi_Km2Pm_PE[0] = KaonP_PE[0] + Sqrt(KaonM_P[0]**2 + mP**2)
      Phi_Km2Pm_P[0]  = Phi_P[0]
      valM2 = Phi_Km2Pm_PE[0]**2-Phi_Km2Pm_P[0]**2
      if(valM2>0):
         Phi_Km2Pm_M[0] = Sqrt(valM2)
      else:
         Phi_Km2Pm_M[0] = -999

      #Km Kp->Pip
      Phi_Kp2Pip_PE[0] = KaonM_PE[0] + Sqrt(KaonP_P[0]**2 + mPi**2)
      Phi_Kp2Pip_P[0]  = Phi_P[0]
      valM2 = Phi_Kp2Pip_PE[0]**2-Phi_Kp2Pip_P[0]**2
      if(valM2>0):
         Phi_Kp2Pip_M[0] = Sqrt(valM2)
      else:
         Phi_Kp2Pip_M[0] = -999


      #Km Kp->Pp
      Phi_Kp2Pp_PE[0] = KaonM_PE[0] + Sqrt(KaonP_P[0]**2 + mP**2)
      Phi_Kp2Pp_P[0]  = Phi_P[0]
      valM2 = Phi_Kp2Pp_PE[0]**2-Phi_Kp2Pp_P[0]**2
      if(valM2>0):
         Phi_Kp2Pp_M[0] = Sqrt(valM2)
      else:
         Phi_Kp2Pp_M[0] = -999



      #Pp Pm->Pim
      Etac_Pm2Pim_PE[0] = ProtonP_PE[0] + Sqrt(ProtonM_P[0]**2 + mPi**2)
      Etac_Pm2Pim_P[0]  = Etac_P[0]
      valM2 = Etac_Pm2Pim_PE[0]**2-Etac_Pm2Pim_P[0]**2
      if(valM2>0):
         Etac_Pm2Pim_M[0] = Sqrt(valM2)
      else:
         Etac_Pm2Pim_M[0] = -999


      #Pm Pp->Pip
      Etac_Pp2Pip_PE[0] = ProtonM_PE[0] + Sqrt(ProtonP_P[0]**2 + mPi**2)
      Etac_Pp2Pip_P[0]  = Etac_P[0]
      valM2 = Etac_Pp2Pip_PE[0]**2-Etac_Pp2Pip_P[0]**2
      if(valM2>0):
         Etac_Pp2Pip_M[0] = Sqrt(valM2)
      else:
         Etac_Pp2Pip_M[0] = -999


      #Pp Pm->Km
      Etac_Pm2Km_PE[0] = ProtonP_PE[0] + Sqrt(ProtonM_P[0]**2 + mK**2)
      Etac_Pm2Km_P[0]  = Etac_P[0]
      valM2 = Etac_Pm2Km_PE[0]**2-Etac_Pm2Km_P[0]**2
      if(valM2>0):
         Etac_Pm2Km_M[0] = Sqrt(valM2)
      else:
         Etac_Pm2Km_M[0] = -999


      #Pm Pp->Kp
      Etac_Pp2Kp_PE[0] = ProtonM_PE[0] + Sqrt(ProtonP_P[0]**2 + mK**2)
      Etac_Pp2Kp_P[0]  = Etac_P[0]
      valM2 = Etac_Pp2Kp_PE[0]**2-Etac_Pp2Kp_P[0]**2
      if(valM2>0):
         Etac_Pp2Kp_M[0] = Sqrt(valM2)
      else:
         Etac_Pp2Kp_M[0] = -999


      #Pp->Kp Pm->Km
      Etac_PpPm2KpKm_PE[0] = Sqrt(ProtonP_P[0]**2 + mK**2) + Sqrt(ProtonM_P[0]**2 + mK**2)
      Etac_PpPm2KpKm_P[0]  = Etac_P[0]
      valM2 = Etac_PpPm2KpKm_PE[0]**2-Etac_PpPm2KpKm_P[0]**2
      if(valM2>0):
         Etac_PpPm2KpKm_M[0] = Sqrt(valM2)
      else:
         Etac_PpPm2KpKm_M[0] = -999




      # 3 body
      PpKK_PE[0] = KaonM_PE[0] + KaonP_PE[0] + ProtonP_PE[0]
      PpKK_P[0]  = Sqrt((ProtonP_PX[0] + KaonM_PX[0] + KaonP_PX[0])**2 + \
                        (ProtonP_PY[0] + KaonM_PY[0] + KaonP_PY[0])**2 + \
                        (ProtonP_PZ[0] + KaonM_PZ[0] + KaonP_PZ[0])**2)
      valM2 = PpKK_PE[0]**2-PpKK_P[0]**2
      if(valM2>0):
         PpKK_M[0] = Sqrt(valM2)
      else:
         PpKK_M[0] = -999


      PpKm_Kp2Pip_PE[0] = ProtonP_PE[0] + KaonM_PE[0] + Sqrt(KaonP_P[0]**2 + mPi**2)
      PpKm_Kp2Pip_P[0]  = PpKK_P[0]
      valM2 = PpKm_Kp2Pip_PE[0]**2-PpKm_Kp2Pip_P[0]**2
      if(valM2>0):
         PpKm_Kp2Pip_M[0] = Sqrt(valM2)
      else:
         PpKm_Kp2Pip_M[0] = -999


      
      PmKK_PE[0] = KaonM_PE[0] + KaonP_PE[0] + ProtonM_PE[0]
      PmKK_P[0]  = Sqrt((ProtonM_PX[0] + KaonM_PX[0] + KaonP_PX[0])**2 + \
                        (ProtonM_PY[0] + KaonM_PY[0] + KaonP_PY[0])**2 + \
                        (ProtonM_PZ[0] + KaonM_PZ[0] + KaonP_PZ[0])**2)
      valM2 = PmKK_PE[0]**2-PmKK_P[0]**2
      if(valM2>0):
         PmKK_M[0] = Sqrt(valM2)
      else:
         PmKK_M[0] = -999

      PmKp_Km2Pim_PE[0] = ProtonM_PE[0] + KaonP_PE[0] + Sqrt(KaonM_P[0]**2 + mPi**2)
      PmKp_Km2Pim_P[0]  = PmKK_P[0]
      valM2 = PmKp_Km2Pim_PE[0]**2-PmKp_Km2Pim_P[0]**2
      if(valM2>0):
         PmKp_Km2Pim_M[0] = Sqrt(valM2)
      else:
         PmKp_Km2Pim_M[0] = -999



      PPKm_PE[0] = ProtonM_PE[0] + ProtonP_PE[0] + KaonM_PE[0]
      PPKm_P[0]  = Sqrt((ProtonP_PX[0] + ProtonM_PX[0] + KaonM_PX[0])**2 + \
                        (ProtonP_PY[0] + ProtonM_PY[0] + KaonM_PY[0])**2 + \
                        (ProtonP_PZ[0] + ProtonM_PZ[0] + KaonM_PZ[0])**2)
      valM2 = PPKm_PE[0]**2-PPKm_P[0]**2
      if(valM2>0):
         PPKm_M[0] = Sqrt(valM2)
      else:
         PPKm_M[0] = -999


      PmKm_Pp2Pip_PE[0] = ProtonM_PE[0] + KaonM_PE[0] + Sqrt(ProtonP_P[0]**2 + mPi**2)
      PmKm_Pp2Pip_P[0]  = PPKm_P[0]
      valM2 = PmKm_Pp2Pip_PE[0]**2-PmKm_Pp2Pip_P[0]**2
      if(valM2>0):
         PmKm_Pp2Pip_M[0] = Sqrt(valM2)
      else:
         PmKm_Pp2Pip_M[0] = -999



      PPKp_PE[0] = ProtonM_PE[0] + ProtonP_PE[0] + KaonP_PE[0]
      PPKp_P[0]  = Sqrt((ProtonP_PX[0] + ProtonM_PX[0] + KaonP_PX[0])**2 + \
                        (ProtonP_PY[0] + ProtonM_PY[0] + KaonP_PY[0])**2 + \
                        (ProtonP_PZ[0] + ProtonM_PZ[0] + KaonP_PZ[0])**2)
      valM2 = PPKp_PE[0]**2-PPKp_P[0]**2
      if(valM2>0):
         PPKp_M[0] = Sqrt(valM2)
      else:
         PPKp_M[0] = -999


      PpKp_Pm2Pim_PE[0] = ProtonP_PE[0] + KaonP_PE[0] + Sqrt(ProtonM_P[0]**2 + mPi**2)
      PpKp_Pm2Pim_P[0]  = PPKp_P[0]
      valM2 = PpKp_Pm2Pim_PE[0]**2-PpKp_Pm2Pim_P[0]**2
      if(valM2>0):
         PpKp_Pm2Pim_M[0] = Sqrt(valM2)
      else:
         PpKp_Pm2Pim_M[0] = -999



      Phi_M_Branch.Fill()
      Phi_P_Branch.Fill()
      Phi_PE_Branch.Fill()


      Etac_M_Branch.Fill()
      Etac_P_Branch.Fill()
      Etac_PE_Branch.Fill()


      Lst1ws_M_Branch.Fill()
      Lst1ws_P_Branch.Fill()
      Lst1ws_PE_Branch.Fill()



      Lst2ws_M_Branch.Fill()
      Lst2ws_P_Branch.Fill()
      Lst2ws_PE_Branch.Fill()



      Lst1_K2pi_M_Branch.Fill()
      Lst1_K2pi_P_Branch.Fill()
      Lst1_K2pi_PE_Branch.Fill()


      Lst2_K2pi_M_Branch.Fill()
      Lst2_K2pi_P_Branch.Fill()
      Lst2_K2pi_PE_Branch.Fill()


      Lst1_K2p_M_Branch.Fill()
      Lst1_K2p_P_Branch.Fill()
      Lst1_K2p_PE_Branch.Fill()


      Lst2_K2p_M_Branch.Fill()
      Lst2_K2p_P_Branch.Fill()
      Lst2_K2p_PE_Branch.Fill()


      Lst1_p2pi_M_Branch.Fill()
      Lst1_p2pi_P_Branch.Fill()
      Lst1_p2pi_PE_Branch.Fill()


      Lst2_p2pi_M_Branch.Fill()
      Lst2_p2pi_P_Branch.Fill()
      Lst2_p2pi_PE_Branch.Fill()


      Lst1_p2K_M_Branch.Fill()
      Lst1_p2K_P_Branch.Fill()
      Lst1_p2K_PE_Branch.Fill()


      Lst2_p2K_M_Branch.Fill()
      Lst2_p2K_P_Branch.Fill()
      Lst2_p2K_PE_Branch.Fill()


      Phi_Km2Pim_M_Branch.Fill()
      Phi_Km2Pim_P_Branch.Fill()
      Phi_Km2Pim_PE_Branch.Fill()


      Phi_Kp2Pip_M_Branch.Fill()
      Phi_Kp2Pip_P_Branch.Fill()
      Phi_Kp2Pip_PE_Branch.Fill()



      PpKK_M_Branch.Fill()
      PpKK_P_Branch.Fill()
      PpKK_PE_Branch.Fill()


      PpKm_Kp2Pip_M_Branch.Fill()
      PpKm_Kp2Pip_P_Branch.Fill()
      PpKm_Kp2Pip_PE_Branch.Fill()



      PmKK_M_Branch.Fill()
      PmKK_P_Branch.Fill()
      PmKK_PE_Branch.Fill()


      PmKp_Km2Pim_M_Branch.Fill()
      PmKp_Km2Pim_P_Branch.Fill()
      PmKp_Km2Pim_PE_Branch.Fill()




      #Kp Km->Pm
      Phi_Km2Pm_PE_Branch.Fill()
      Phi_Km2Pm_P_Branch.Fill()
      Phi_Km2Pm_M_Branch.Fill()


      #Km Kp->Pp
      Phi_Kp2Pp_PE_Branch.Fill()
      Phi_Kp2Pp_P_Branch.Fill()
      Phi_Kp2Pp_M_Branch.Fill()



      #Pp Pm->Pim
      Etac_Pm2Pim_PE_Branch.Fill()
      Etac_Pm2Pim_P_Branch.Fill()
      Etac_Pm2Pim_M_Branch.Fill()


      #Pm Pp->Pip
      Etac_Pp2Pip_PE_Branch.Fill()
      Etac_Pp2Pip_P_Branch.Fill()
      Etac_Pp2Pip_M_Branch.Fill()


      #Pp Pm->Km
      Etac_Pm2Km_PE_Branch.Fill()
      Etac_Pm2Km_P_Branch.Fill()
      Etac_Pm2Km_M_Branch.Fill()


      #Pm Pp->Kp
      Etac_Pp2Kp_PE_Branch.Fill()
      Etac_Pp2Kp_P_Branch.Fill()
      Etac_Pp2Kp_M_Branch.Fill()


      #Pp->Kp Pm->Km
      Etac_PpPm2KpKm_PE_Branch.Fill()
      Etac_PpPm2KpKm_P_Branch.Fill()
      Etac_PpPm2KpKm_M_Branch.Fill()



      PPKm_PE_Branch.Fill()
      PPKm_P_Branch.Fill()
      PPKm_M_Branch.Fill()


      PmKm_Pp2Pip_PE_Branch.Fill()
      PmKm_Pp2Pip_P_Branch.Fill()
      PmKm_Pp2Pip_M_Branch.Fill()



      PPKp_PE_Branch.Fill()
      PPKp_P_Branch.Fill()
      PPKp_M_Branch.Fill()


      PpKp_Pm2Pim_PE_Branch.Fill()
      PpKp_Pm2Pim_P_Branch.Fill()
      PpKp_Pm2Pim_M_Branch.Fill()


      

#   newtree.Print()
   newfile.Write()
   newfile.Close()




jobIDs = [556,557,565,566]
for jobID in jobIDs:
   for i in range(0,600):
      filename = "/eos/user/a/ausachov/Data_LstLst/"+str(jobID)+"/"+str(i)+"/Tuple.root"
      newfilename = "/eos/user/a/ausachov/Data_LstLst/"+str(jobID)+"/"+str(i)+"/TupleSelUnTrigger.root"
      try:
         fillNewTuple(filename,newfilename)
         print jobID, i, "processed \n"
      except:
         pass











