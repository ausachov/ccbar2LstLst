from ROOT import *

cutTrigger = "(Jpsi_L0HadronDecision_TOS || Jpsi_L0Global_TIS)"
cutTrack = "ProtonP_TRACK_CHI2NDOF<3 && ProtonM_TRACK_CHI2NDOF<3 && KaonP_TRACK_CHI2NDOF<3 && KaonM_TRACK_CHI2NDOF<3"
cutVtx = "Lst1_ENDVERTEX_CHI2<4 && Lst2_ENDVERTEX_CHI2<4 && Jpsi_ENDVERTEX_CHI2<20"
cutPID = "ProtonP_ProbNNp>0.4 && ProtonM_ProbNNp>0.4 && KaonP_ProbNNk>0.1 && KaonM_ProbNNk>0.1"
cutIP = "ProtonP_IPCHI2_OWNPV>16 && ProtonM_IPCHI2_OWNPV>16 && KaonP_IPCHI2_OWNPV>16 && KaonM_IPCHI2_OWNPV>16"
cutP = "ProtonP_P>10000 && ProtonM_P>10000"

cutPhiVeto = "abs(Phi_M-1020)>10 && abs(Lst1_p2K_M-1020)>10 && abs(Lst2_p2K_M-1020)>10 && abs(Etac_PpPm2KpKm_M-1020)>10"
cutLstM = "Lst1_M>1500 && Lst2_M>1500 && Lst1_M<1540 && Lst2_M<1540"

cutDef_v0 = cutTrigger+" && "+cutTrack+" && "+cutVtx+" && "+cutPID+" && "+cutIP+" && "+cutP+" && "+cutPhiVeto

ch = TChain("DecayTree")
ch.Add("LstLst_RunII_merged.root")
h = TH1F("h","h",150,3000,4500)
h1 = TH1F("h1","h1",150,3000,4500)


cutTightPID = "ProtonP_ProbNNk<0.9 && ProtonM_ProbNNk<0.9 && KaonP_ProbNNk>0.4 && KaonM_ProbNNk>0.4"

#f = TFile("LstLst_RunII_cutDef_tightPID.root","recreate")
#tree = ch.CopyTree(cutDef_v0+" && "+cutTightPID)

f = TFile("LstLst_RunII_cutDef_v0.root","recreate")
tree = ch.CopyTree(cutDef_v0)

f.Write()
f.Close()

#ch.Draw("Lst1_M",cut0+" && "+cutPT)
