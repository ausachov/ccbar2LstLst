from ROOT import gROOT, TChain, TCut, TFile, TTree, TProof

#TProof.Open()
chain = TChain("DecayTree")
#chain.SetProof()

chain.Add("Reduced_LstLstDetached_2016*")





#'TRCHI2DOF'        :    3.  ,
#    'CcbarPT'          :    3000,   #MeV
#        'KaonProbNNk'      :    0.1  ,
#        'KaonPT'           :    600. , # MeV
#        'KaonPTSec'        :    350. , # MeV
#        'ProtonProbNNp'    :    0.1  ,
#        'ProtonPT'         :    800.  , # MeV
#        'ProtonPTSec'      :    500.  , # MeV
#        'LstVtxChi2'       :    16.  ,
#        'LstMinMass'       :  1440  ,
#        'LstMaxMass'       :  1600  ,
#        'CombMaxMass'      :  6100.  , # MeV, before Vtx fit
#        'CombMinMass'      :  3000.  , # MeV, before Vtx fit
#        'MaxMass'          :  6000.  , # MeV, after Vtx fit
#        'MinMass'          :  2950.  , # MeV, after Vtx fit
#        'Lst_TisTosSpecs'  : { "Hlt1Global%TIS" : 0 }


cutID = TCut("")






totCut = TCut(cutID)

newfile = TFile("Tighten_LstLstDetached_2016.root","recreate")

newtree=TTree()
newtree.SetMaxTreeSize(5000000000)
newtree = chain.CopyTree(totCut.GetTitle())

newtree.Print()

newtree.GetCurrentFile().Write() 
newtree.GetCurrentFile().Close()

#newfile.Write()
